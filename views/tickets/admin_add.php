<div id="feedback" class="parallax-container">
    <div class="container">
        <div class="row">
            <div class="">
                <form action="" method="post">
                    <div class="card-content white-text">
                        <h4 class="card-title">Добавление билета</h4>
                        <div class="input-field col l6">

                            <input class="form-input validate" id="whence" name="whence" type="text">
                            <label for="whence">Откуда</label>
                        </div>

                        <div class="input-field col l6">
                            <input class="form-input validate" id="where" name="where" type="text">
                            <label for="where">Куда</label>
                        </div>
                        <div class="input-field col s12 l6  ">
                            <label for="departure_date">Дата вылета</label>
                            <input type="date" name="departure_date" class="datepicker white-text" id="departure_date">
                        </div>
                        <div class="input-field col s12 l6  ">
                            <label for="departure_time"></label>
                            <input type="time" name="departure_time" class="white-text" id="departure_time">
                        </div>
                        <div class="input-field col s12 l6">
                            <label for="date_of_arrival">Прилёт</label>
                            <input type="date" name="date_of_arrival" class="datepicker white-text" id="date_of_arrival">
                        </div>
                        <div class="input-field col s12 l6  ">
                            <label for="time_of_arrival"></label>
                            <input type="time" name="time_of_arrival" class="white-text" id="time_of_arrival">
                        </div>
                        <div class="input-field col l6">
                            <input class="form-input validate" id="airline" name="airline" type="text">
                            <label for="airline">airline</label>
                        </div>
                        <div class="input-field col l6">
                            <input class="form-input validate" id="price" name="price" type="text">
                            <label for="price">Цена</label>
                        </div>
                        <div class="input-field col l6">
                            <input class="form-input validate" id="count_tickets" name="count_tickets" type="number">
                            <label for="count_tickets">Кол-во билетов</label>
                        </div>
                        <div class="input-field col l6"><br><br><br><br></div>
                        <button type="submit" class="waves-effect waves-light btn right teal lighten-2" id="form-send">
                            <i class="material-icons right">trending_flat</i>Отправить
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <div class="parallax"><img src="/img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div>