<div id="feedback" class="parallax-container">
    <div class="container">
        <div class="row">
            <div class="">
                <div class="card-content white-text">
                    <form action="" method="post">
                        <h4 class="card-title">Бронирование билета</h4>
                        <div class="input-field">
                            <input id="fio" type="text" class="validate white-text" name="fio">
                            <label for="fio">Ф.И.О</label>
                        </div>
                        <div class="input-field ">
                            <input id="email" type="email" class="validate white-text" name="email">
                            <label for="email">Email</label>
                        </div>
                        <div class="input-field ">
                            <input id="count_tickets" type="number" class="validate white-text" name="count_tickets">
                            <label for="count_tickets">Кол-во билетов</label>
                        </div>
                        <input type="number" hidden value="<?=$data['id']?>" name="id">
                        <button type="submit" class="waves-effect waves-light btn right teal lighten-2"><i
                                    class="material-icons right">trending_flat</i>Отправить
                        </button>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <div class="parallax"><img src="/img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div>