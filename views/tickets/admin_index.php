<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>
<!--search-->
<!--<div   class="parallax-container">
    <div class="row container">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-image">
                    <div class="card-title">
                        <a href="/admin/tickets/add" class="btn">Новый рейс</a>
                    </div>
                </div>
                <div class="card-content">
                    <table class="black-text bordered" id="example  ">
                        <th>#</th>
                        <th>Откуда</th>
                        <th>Куда</th>
                        <th>Дата вылета</th>
                        <th>Дата прилёта</th>
                        <th>AIRLINE</th>
                        <th>Цена</th>
                        <th>Кол-во билетов</th>
                        <th>Черновик</th>
                        <th>Действия</th>
                        <tbody class="black-text">
                        <?/* foreach ($data['tickets'] as $ticket) { */?>
                            <tr>
                                <td><?/*=$ticket['ID'] */?></td>
                                <td><?/*=$ticket['WHENCE'] */?></td>
                                <td><?/*=$ticket['WHITHER'] */?></td>
                                <td><?/*=$ticket['DEPARTURE_DATE'] */?></td>
                                <td><?/*=$ticket['DATE_OF_ARRIVAL'] */?></td>
                                <td><?/*=$ticket['AIRLINE'] */?></td>
                                <td><?/*=$ticket['PRICE'] */?></td>
                                <td><?/*=$ticket['COUNT_TICKETS'] */?></td>
                                <td><?/*=$ticket['FLAG_VIEW'] */?></td>
                                <td><a href="/admin/tickets/delete/<?/*=$ticket['ID']*/?>">Удалить</a>
                                    <br><a href="/admin/tickets/edit/<?/*=$ticket['ID']*/?>">Редактировать</a></td>
                            </tr>
                        <?php /*} */?>
                        </tbody>
                    </table>
                </div>

            </div>
            <?/*=$data['page_links']*/?>
        </div>
    </div>
    <div class="parallax"><img src="/img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div>-->
<!--/search-->

<!--search-->
<div class="parallax-container ">
    <div class="row ">
        <div class="col s12 m12 l12">
            <div class="card">

                <div class="card-content">
                    <table class=" black-text centered bordered  " id="example">
                        <thead>
                        <tr>
                            <th>Откуда</th>
                            <th>Куда</th>
                            <th>Дата вылета</th>
                            <th>Дата прибытия</th>
                            <th>Авиакомпания</th>
                            <th>Цена</th>
                            <th>Кол-во билетов</th>
                            <th>Действия</th>
                        </tr>
                        </thead>

                        <tbody>
                        <? foreach ($data['tickets'] as $ticket) { ?>
                            <tr>
                                <td><?= $ticket['WHENCE'] ?></td>
                                <td><?= $ticket['WHITHER'] ?> </td>
                                <td><?= $ticket['DEPARTURE_DATE'] ?> в <?= $ticket['DEPARTURE_TIME'] ?></td>
                                <td><?= $ticket['DATE_OF_ARRIVAL'] ?> в <?= $ticket['TIME_OF_ARRIVAL'] ?></td>
                                <td><?= $ticket['AIRLINE'] ?></td>
                                <td><?= $ticket['PRICE'] ?></td>
                                <td><?= $ticket['COUNT_TICKETS'] ?></td>
                                <td>
                                    <a href="/admin/tickets/edit/<?= $ticket['ID'] ?>">Редактировать</a><br>
                                    <a href="/admin/tickets/delete/<?= $ticket['ID'] ?>">Удалить</a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>

                </div>
                <br>
                <br>
                <br>
                <div class="card-image">
                    <div class="card-title">
                        <a href="/admin/tickets/add" class="btn">Новый рейс</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="/img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div>


<!--/search-->


