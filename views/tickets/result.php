<!--search-->
<div class="parallax-container ">
            <? if (!$data['tickets']) { ?>
                <h1 class="center-align">
                    Ничего не найдено
                </h1>
            <? } else { ?>
            <table class=" black-text centered bordered  " id="example">
                <thead>
                <tr>
                    <th>Откуда</th>
                    <th>Куда</th>
                    <th>Дата вылета</th>
                    <th>Авиакомпания</th>
                    <th>Цена</th>
                    <th>Кол-во билетов</th>
                    <th>Действия</th>
                </tr>
                </thead>

                <tbody>
                <? foreach ($data['tickets'] as $ticket) { ?>
                    <tr>
                        <td><?= $ticket['WHENCE'] ?></td>
                        <td><?= $ticket['WHITHER'] ?> </td>
                        <td><?= $ticket['DEPARTURE_DATE'] ?> в <?= $ticket['DEPARTURE_TIME'] ?></td>
                        <td><?= $ticket['AIRLINE'] ?></td>
                        <td><?= $ticket['PRICE'] ?></td>
                        <td><?= $ticket['COUNT_TICKETS'] ?></td>
                        <td>
                            <a href="/tickets/reservation/<?= $ticket['ID'] ?>">Бронировать</a><br>
                            <a href="/tickets/buy/<?= $ticket['ID'] ?>">Купить</a>
                        </td>
                    </tr>
                <? } ?>


                </tbody>
            </table>




    <? } ?>
<div class="parallax"><img src="/img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div><script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>

<!--/search-->

