<div id="feedback" class="parallax-container">
    <div class="container">
        <div class="row">
            <div class="">
                <div class="card-content white-text">
                   <h4 class="center-align">Вы успешно забронировали билет</h4>
                   <h5 class="center-align">Проверьте свой электронный ящик</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="/img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div>