<!--search-->
<div id="search" class="parallax-container">
    <div class="container">
        <h2 class="white-text">Поиск авиабилетов</h2>
        <br>
        <form action="/tickets/result" method="post">
            <div class="row">
                <div class="input-field col s12 l4">
                    <input id="whence" type="text" name="whence" class="validate white-text">
                    <label for="whence">Откуда</label>
                </div>
                <div class="input-field col s12 l4">
                    <input id="when" type="text" name="where" class="validate white-text">
                    <label for="when">Куда</label>
                </div>
                <div class="input-field col s12 l4">
                    <label for="datepicker">Когда</label>
                    <input type="date" name="date" class="datepicker white-text" id="datepicker">
                </div>
            </div>
            <button class="btn waves-effect waves-light right" type="submit">Поиск
                <i class="material-icons right">search</i>
            </button>
            <br>
            <br>
            <a class="btn waves-effect waves-light right" type="submit" href="/tickets/lastflight">Последние добавленные
                <i class="material-icons right">search</i>
            </a>
        </form>
    </div>
    <div class="parallax"><img src="/img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div>
<!--/search-->


