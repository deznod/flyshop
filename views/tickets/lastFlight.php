<div class="parallax-container ">
    <div class="row ">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-image">
                    <div class="card-title">
                        <a href="/tickets">
                            <img src="/img/001-left-arrow.png" alt="Вернуться к поиску" style="width:60px;"></a>
                    </div>
                </div>
                <div class="card-content">
                    <table class=" black-text centered bordered  " id="example">
                        <thead>
                        <tr>
                            <th>Откуда</th>
                            <th>Куда</th>
                            <th>Дата вылета</th>
                            <th>Авиакомпания</th>
                            <th>Цена</th>
                            <th>Кол-во билетов</th>
                            <th>Действия</th>
                        </tr>
                        </thead>

                        <tbody>
                        <? foreach ($data['tickets'] as $ticket) { ?>
                            <tr>
                                <td><?= $ticket['WHENCE'] ?></td>
                                <td><?= $ticket['WHITHER'] ?> </td>
                                <td><?= $ticket['DEPARTURE_DATE'] ?> в <?= $ticket['DEPARTURE_TIME'] ?></td>
                                <td><?= $ticket['AIRLINE'] ?></td>
                                <td><?= $ticket['PRICE'] ?></td>
                                <td><?= $ticket['COUNT_TICKETS'] ?></td>
                                <td>
                                    <a href="/tickets/reservation/<?= $ticket['ID'] ?>">Бронировать</a><br>
                                    <a href="/tickets/buy/<?= $ticket['ID'] ?>">Купить</a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
    <div class="parallax"><img src="/img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div>
<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>