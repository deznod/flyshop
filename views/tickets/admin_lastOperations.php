<div class="parallax-container ">
    <div class="row ">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-image">

                </div>
                <div class="card-content">
                    <table class=" black-text centered bordered  " id="example">
                        <thead>
                        <tr>
                            <th>Ф.И.О.</th>
                            <th>Откуда</th>
                            <th>Куда</th>
                            <th>Дата вылета</th>
                            <th>Кол-во билетов</th>
                        </tr>
                        </thead>
                        <tbody>

                        <? foreach ($data['tickets'] as $ticket) { ?>
                            <tr>
                                <td><?= $ticket['USER'] ?></td>
                                <td><?= $ticket['WHENCE'] ?></td>
                                <td><?= $ticket['WHITHER'] ?> </td>
                                <td><?= $ticket['DEPARTURE_DATE'] ?> в <?= $ticket['DEPARTURE_TIME'] ?></td>
                                <td><?= $ticket['COUNT_TICKETS'] ?></td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
    <div class="parallax"><img src="/img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div>
<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>