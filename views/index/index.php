

<!--index-content-->
<div class="slider fullscreen">
    <ul class="slides">
        <li>
            <img src="img/air/samolet-nebo-minimalizm.jpg"> <!-- random image -->
            <div class="caption center-align">
                <h3>Fly Shop</h3>
                <h5 class="light grey-text text-lighten-3">Это надёжно.</h5>
            </div>
        </li>
        <li>
            <img src="img/air/33997259.jpg"> <!-- random image -->
            <div class="caption left-align">
                    <h3>Качество обслуживания</h3>
                <h5 class="light grey-text text-lighten-3"></h5>
            </div>
        </li>
        <li>
            <img src="img/air/Заказ-авиабилетов-из-Ставрополя1.jpg"> <!-- random image -->
            <div class="caption right-align">
                <h3>Понятное использование</h3>
                <h5 class="light grey-text text-lighten-3"></h5>
            </div>
        </li>
        <li>
            <img src="img/air/samolet-passazhirskiy-2409.jpg"> <!-- random image -->
            <div class="caption center-align">
                <h3>Безопасность</h3>
                <h5 class="light grey-text text-lighten-3"></h5>
            </div>
        </li>
    </ul>
</div>
<!--/index-content-->


<!--  Scripts-->
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>

</body>
</html>
