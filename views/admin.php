<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Admin Panel</title>

    <!-- CSS  -->
    <script src="/js/jquery-2.1.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

</head>
<body>


<!--nav-bars-->
<nav class="teal lighten-2" role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="#" class="brand-logo white-text text-lighten-1"></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="/admin/tickets">Бронирование</a></li>
            <li><a href="/admin/feedback">Обратная связь</a></li>
            <li><a href="/admin/tickets/lastoperations">Последние операции по бронированию</a></li>
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li><a href="/admin/tickets">Бронирование</a></li>
            <li><a href="/admin/feedback">Обратная связь</a></li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>
<!--/nav-bars-->

<?php if(Session::hasFlash()){?>
    <div class="alert alert-info" role="alert">
        <?php Session::flash();?>
    </div>
<?}?>
<?=$data['content']?>
<script src="/js/materialize.js"></script>
<script src="/js/init.js"></script>

</body>
</html>
