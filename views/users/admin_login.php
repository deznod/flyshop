<div id="feedback" class="parallax-container">
    <div class="container">
        <div class="row">
            <div class="">
                <form action="" method="post">
                    <div class="card-content white-text">
                        <h4 class="card-title">Админ панель</h4>
                        <div class="input-field">

                            <input class="form-input validate" id="login" name="login" type="text">
                            <label for="login">Login</label>
                        </div>
                        <div class="input-field ">
                            <input class="form-input validate" id="password" name="password" type="password">
                            <label for="password">Password</label>
                        </div>
                        <button type="submit" class="waves-effect waves-light btn right teal lighten-2" id="form-send">
                            <i class="material-icons right">trending_flat</i>Отправить
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <div class="parallax"><img src="/img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div>