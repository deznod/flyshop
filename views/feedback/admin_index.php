<div class="parallax-container ">
    <div class="row ">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-image">
                    <div class="card-title">
                        <a href="/tickets">
                            <img src="/img/001-left-arrow.png" alt="Вернуться к поиску" style="width:60px;"></a>
                    </div>
                </div>
                <div class="card-content">
                    <table class=" black-text centered bordered  " id="example">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Email</th>
                            <th>Сообщение</th>
                            <th>Действия</th>
                        </tr>
                        </thead>

                        <tbody>
                        <? foreach ($data['messages'] as $message) { ?>
                            <tr>
                                <td><?= $message['id'] ?></td>
                                <td><?= $message['email'] ?></td>
                                <td><?= $message['text_message'] ?></td>
                                <td>
                                    <a href="/admin/feedback/del/<?=$message['id']?>">
                                        Удалить
                                    </a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
    <div class="parallax"><img src="/img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div>
<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>