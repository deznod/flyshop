<div id="feedback" class="parallax-container">
    <div class="container">
        <div class="row">
            <form class="form" method="post">
                <div class="card-content white-text">
                    <h4 class="card-title">Обратная связь</h4>
                    <div class="input-field">
                        <input id="email" type="email" class="validate white-text" name="email">
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field ">
                        <textarea id="textarea1" class="materialize-textarea" name="text"></textarea>
                        <label for="textarea1">Текст сообщения</label>
                    </div>
                    <input type="submit" class="waves-effect waves-light btn right teal lighten-2" value="Отправить">
                </div>
            </form>
        </div>

    </div>
    <div class="parallax"><img src="img/air/nebo-samoliot-priroda.jpg" alt="Unsplashed background img 1"></div>
</div>