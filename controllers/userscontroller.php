<?php


class usersController extends Controller
{

    /**
     * usersController constructor.
     * @param array $data
     */
    public function __construct($data = array())
    {
        parent::__construct($data);
        $this->model = new Admins();
    }
    public function admin_login(){
        if ($_POST && isset($_POST['login']) && isset($_POST['password'])){
            $user = $this->model->getByLogin($_POST['login']);

            if ($user && $user['PASSWORD']==$_POST['password']){
                Session::set('login',$user['LOGIN']);
                Session::set('role','admin');
            }
           Router::redirect('/admin/tickets');

        }
    }
    public function admin_logout(){
        Session::destroy();
        Router::redirect('/admin/');

    }
}