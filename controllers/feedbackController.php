<?php


class feedbackController extends Controller
{
    public function __construct($data = array())
    {
        parent::__construct($data);
        $this->model = new Message();
    }

    public function index()
    {
        if ($_POST) {
            $this->model->addMessage($_POST);
            Session::setFlash('fwfwfw');
        }
    }

    public function admin_index()
    {
        $this->data['messages'] = $this->model->get_all_messages();
    }

    public function admin_del()
    {
        if (isset($this->params[0])) {
            $id = $this->params[0];
            $this->model->del_message_by_id($id);
            Router::redirect('/admin/feedback/');
        }
    }
}