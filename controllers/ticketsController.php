<?php


class ticketsController extends Controller
{

    /**
     * searchController constructor.
     * @param array $data
     */
    public function __construct($data = array())
    {
        parent::__construct($data);
        $this->model = new tickets_model();
    }

    public function reservation(){
        if (isset($this->params[0])) {
            $id = $this->params[0];
            $this->data['id'] = $id;
        }
        if ($_POST){
            $this->model->reservation($_POST);
            Router::redirect('/tickets/success_reservation');
        }
    }
    public function buy(){
        if (isset($this->params[0])) {
            $id = $this->params[0];
            $this->data['id'] = $id;
        }
        if ($_POST){
            $this->model->buy($_POST);
            Router::redirect('/tickets/success_buy');
        }
    }
    public function success_reservation(){


    }
    public function success_buy(){


    }
    public function index()
    {

    }

    public function result()
    {
        $this->data['tickets'] = $this->model->search($_POST);

        /*$data = $this->model->search($_POST);
        $this->data['tickets'] = $data;*/
    }
    public function lastFlight(){

        $this->data['tickets'] = $this->model->lastFlight();
    }


    public function admin_index(){
        $this->data['tickets'] = $this->model->get_all();
    }
    public function admin_add(){
        if ($_POST){
            $this->model->add($_POST);
            Router::redirect('/admin/tickets/');
        }

    }
    public function admin_delete(){
        if (isset($this->params[0])){
            $id = $this->params[0];
            $this->model->delete($id);
        }
        Router::redirect('/admin/tickets/');
    }
    public function admin_edit(){
        $id = $this->params[0];
        if (isset($this->params[0])){
            $id = $this->params[0];
            $this->data['tickets'] = $this->model->get_element($id);
        }
        if ($_POST){
            $this->model->add($_POST,$id);
            Router::redirect('/admin/tickets/');
        }
    }
    public function admin_lastOperations(){
        $this->data['tickets'] = $this->model->lastOperations();
    }
}