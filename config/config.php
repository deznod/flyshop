<?php

Config::set('site_name', 'FlyShop');



//Routes, Route name => method prefix
Config::set('routes', array(
    'default' => '',
    'admin' => 'admin_',
));

Config::set('default_route', 'default');
Config::set('default_language', 'en');
Config::set('default_controller', 'index');
Config::set('default_action', 'index');

/*Config::set('db.host','185.41.186.236');
Config::set('db.user','museumgimn1');
Config::set('db.password','q1w2e3r4t5');
Config::set('db.name','db_museumgimn1');*/
Config::set('db.host', 'localhost');
Config::set('db.user', 'root');
Config::set('db.password', '');
Config::set('db.name', 'flyshop');

Config::set('salt', 'hxRa06am');

