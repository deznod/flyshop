-- we don't know how to generate schema flyshop (class Schema) :(
-- Нужно создать базу flyshop и выполнить sql этот скрипт
-- админка /admin
-- лонин: admin
-- пароль: admin

create table if not exists admins
(
  ID int auto_increment,
  LOGIN varchar(25) not null,
  PASSWORD varchar(25) not null,
  constraint `PRIMARY`
  primary key (ID)
)
;

create table if not exists feedback
(
  ID int auto_increment,
  email varchar(25) not null,
  text_message text not null,
  constraint `PRIMARY`
  primary key (ID)
)
;

create table if not exists flight
(
  ID int auto_increment,
  WHENCE varchar(20) not null,
  DEPARTURE_DATE varchar(25) not null,
  AIRLINE varchar(20) not null,
  PRICE double not null,
  COUNT_TICKETS int not null,
  FLAG_VIEW tinyint(1) default '1' not null,
  WHITHER varchar(20) not null,
  DATE_OF_ARRIVAL varchar(25) not null,
  DEPARTURE_TIME varchar(25) not null,
  TIME_OF_ARRIVAL varchar(25) not null,
  constraint `PRIMARY`
  primary key (ID)
)
;

create table if not exists buy_list
(
  ID int auto_increment,
  USER varchar(30) not null,
  email varchar(25) not null,
  tel varchar(25) not null,
  COUNT_TICKETS_USER int(5) not null,
  TICKET int(5) not null,
  constraint `PRIMARY`
  primary key (ID),
  constraint buy_list_ibfk_1
  foreign key (TICKET) references flight (ID)
)
;

create index TICKET
  on buy_list (TICKET)
;

create table if not exists reservation_list
(
  ID int auto_increment,
  USER varchar(30) not null,
  COUNT_TICKETS_USER int(5) not null,
  TICKET int(5) not null,
  email varchar(25) not null,
  constraint `PRIMARY`
  primary key (ID),
  constraint reservation_list_ibfk_1
  foreign key (TICKET) references flight (ID)
)
;

create index TICKET
  on reservation_list (TICKET)
;

insert into admins(LOGIN, PASSWORD) values ('admin', 'admin')