<?php

/**
 * Класс для создания пагинации
 * @author Дмитий Кузьмин - -
 * @version 1.0
 * @date
 */
class Pagination
{

    /**
     * Колличество эллементов на странице
     *
     * @var
     */
    private $per_Page;

    /**
     * Параметр для извлечения номера страницы
     *
     * @var
     */
    private $instance;

    /**
     * Номер страницы
     *
     * @var
     */
    private $page;

    /**
     * Предел для источника данных
     *
     * @var
     */
    private $limit;

    /**
     * Общее количество записей
     *
     * @var int
     */
    private $totalRows = 0;


    /**
     * Pagination constructor.
     * @param $per_Page - количество элементов на странице
     * @param $instance - экземпляр для параметра GET
     */
    public function __construct($per_Page, $instance)
    {
        $this->instance = $instance;
        $this->per_Page = $per_Page;
        $this->set_instance();
    }

    /**
     * get_start
     *
     * Начальная точка ограничения для базы данных
     *
     * @return mixed начальную точку ограничения для базы данных
     */
    public function get_start()
    {
        return ($this->page * $this->per_Page) - $this->per_Page;
    }

    /**
     * set_instance
     *
     * устанавливает параметр экземпляра, если числовое значение равно 0, то устанавливается в 1
     */
    private function set_instance()
    {
        $this->page = (int)(!isset($_GET[$this->instance]) ? 1 : $_GET[$this->instance]);
        $this->page = ($this->page == 0 ? 1 : $this->page);
    }

    /**
     * @param $totalRows
     */
    public function set_total($totalRows)
    {
        $this->totalRows = $totalRows;
    }

    /**
     * @return string - лимит для запроса в базу данных
     */
    public function get_limit()
    {
        return "LIMIT " . $this->get_start() . ",$this->per_Page";
    }

    /**
     * page_links
     * Создаёт HTML-ссылки для навигации по набору данных из базы
     *
     * @param string $path дополнительно задать путь для ссылки
     * @param null $ext дополнительно передаёт дополнительные параметры для _GET
     * @return string
     */
    public function page_links($path = '?', $ext = null)
    {
        $adjacents = 2;
        $prev = $this->page - 1;
        $next = $this->page + 1;
        // print_r($this->_totalRows);
        // print_r($this->_perPage);
        $lastpage = ceil($this->totalRows / $this->per_Page);
        if ($_GET[$this->instance] > $lastpage) {
            header("Location: /");
        }
        $lpm1 = $lastpage - 1;

        $pagination = "";
        if ($lastpage > 1) {
            $pagination .= "<ul class='pagination'>";

            if ($this->page > 1)
                $pagination .= "<li class='pagination__item'><a class='pagination__link pagination__link-arrow-left' href='" . $path . "$this->instance=$prev" . "$ext'></a></li>";
            /*else
                $pagination .= "<span class='disabled'>Previous</span>";*/


            if ($lastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $this->page)
                        $pagination .= "<li class='pagination__item'><a class='pagination__link pagination__link_pick'>$counter</a></li>";
                    else
                        $pagination .= "<li class='pagination__item'><a class='pagination__link' href='" . $path . "$this->instance=$counter" . "$ext'>$counter</a></li>";
                }
            } elseif ($lastpage > 5 + ($adjacents * 2)) {
                if ($this->page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $this->page)
                            $pagination .= "<li class='pagination__item'><a class='pagination__link pagination__link_pick'>$counter</a></li>";
                        else
                            $pagination .= "<li class='pagination__item'><a class='pagination__link' href='" . $path . "$this->instance=$counter" . "$ext'>$counter</a></li>";
                    }
                    $pagination .= "<li class='pagination__item'><a class='pagination__link' href=''>...</a></li>";
                    $pagination .= "<li class='pagination__item'><a class='pagination__link' href='" . $path . "$this->instance=$lpm1" . "$ext'>$lpm1</a></li>";
                    $pagination .= "<li class='pagination__item'><a class='pagination__link' href='" . $path . "$this->instance=$lastpage" . "$ext'>$lastpage</a></li>";
                } elseif ($lastpage - ($adjacents * 2) > $this->page && $this->page > ($adjacents * 2)) {
                    $pagination .= "<li class='pagination__item'><a class='pagination__link' href='" . $path . "$this->instance=1" . "$ext'>1</a></li>";
                    $pagination .= "<li class=pagination__item''><a class='pagination__link' href='" . $path . "$this->instance=2" . "$ext'>2</a></li>";
                    $pagination .= "<li class='pagination__item'><a class='pagination__link' href=''>...</a></li>";
                    for ($counter = $this->page - $adjacents; $counter <= $this->page + $adjacents; $counter++) {
                        if ($counter == $this->page)
                            $pagination .= "<li class='pagination__item'><a href='' class='pagination__link pagination__link_pick'>$counter</a></li>";
                        else
                            $pagination .= "<li class='pagination__item'><a class='pagination__link' href='" . $path . "$this->instance=$counter" . "$ext'>$counter</a></li>";
                    }
                    $pagination .= "<li class='pagination__item'><a class='pagination__link' href=''>...</a></li>";
                    $pagination .= "<li class='pagination__item'><a class='pagination__link' href='" . $path . "$this->instance=$lpm1" . "$ext'>$lpm1</a></li>";
                    $pagination .= "<li class='pagination__item'><a class='pagination__link' href='" . $path . "$this->instance=$lastpage" . "$ext'>$lastpage</a></li>";
                } else {
                    $pagination .= "<li class='pagination__item'><a class='pagination__link' href='" . $path . "$this->instance=1" . "$ext'>1</a></li>";
                    $pagination .= "<li class='pagination__item'><a class='pagination__link' href='" . $path . "$this->instance=2" . "$ext'>2</a></li>";
                    $pagination .= "<li class='pagination__item'><a class='pagination__link' href=''>...</a></li>";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $this->page)
                            $pagination .= "<li class='pagination__item'><a  href='' class='pagination__link pagination__link_pick'>$counter</a></li>";
                        else
                            $pagination .= "<li class='pagination__item'><a class='pagination__link' href='" . $path . "$this->instance=$counter" . "$ext'>$counter</a></li>";
                    }
                }
            }

            if ($this->page < $counter - 1)
                $pagination .= "<li class='pagination__item'><a class='pagination__link pagination__link-arrow-right' href='" . $path . "$this->instance=$next" . "$ext'></a></li>";
            /*else
                $pagination .= "<li><span class='disabled'>Next</span></li>";*/
            $pagination .= "</ul>\n";
        }


        return $pagination;
    }
}
