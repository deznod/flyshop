<?php


class tickets_model extends Model
{
    public function search($data, $limit = '')
    {
        $whence = $data['whence'];
        $where = $data['where'];
        $date = $data['date'];
        $sql = '';
        if (!empty($whence) && !empty($where) && !empty($date)) {
            $sql = "select * from flyshop.flight WHERE flight.WHENCE = '{$whence}' 
                                                  AND  flight.WHITHER = '{$where}'
                                                  AND  flight.DEPARTURE_DATE = '{$date}'";
        } else if (!empty($whence) && empty($where) && empty($date)) {
            $sql = "select * from flyshop.flight WHERE flight.WHENCE = '{$whence}' ";
        } else if (!empty($whence) && !empty($where) && empty($date)) {
            $sql = "select * from flyshop.flight WHERE flight.WHENCE = '{$whence}' 
                                                  AND  flight.WHITHER = '{$where}'";
        } else if (empty($whence) && !empty($where) && empty($date)) {
            $sql = "select * from flyshop.flight WHERE flight.WHITHER = '{$where}'";
        } else if (empty($whence) && !empty($where) && !empty($date)) {
            $sql = "select * from flyshop.flight AND flight.WHITHER = '{$where}'
                                                 AND  flight.DEPARTURE_DATE = '{$date}'";
        } else if (!empty($whence) && empty($where) && !empty($date)) {
            $sql = "select * from flyshop.flight WHERE flight.WHENCE = '{$whence}'
                                                AND  flight.DEPARTURE_DATE = '{$date}'";
        } else {
            return false;
        }
        if ($limit) {
            $sql .= " {$limit}";
        }
        return $this->db->query($sql);
    }

    public function get_all_count()
    {
        $sql = "SELECT COUNT(*) FROM flyshop.flight";
        $result = $this->db->query($sql);
        return $result[0]['COUNT(*)'];
    }

    public function get_all($limit = '')
    {
        $sql = "select * from flyshop.flight WHERE 1 ORDER BY ID DESC";

        if ($limit) {
            $sql .= " {$limit}";
        }
        return $this->db->query($sql);
    }

    public function add($data, $id = '')
    {
        $whence = $data['whence'];
        $where = $data['where'];
        $departure_date = $data['departure_date'];
        $departure_time = $data['departure_time'];
        $date_of_arrival = $data['date_of_arrival'];
        $time_of_arrival = $data['time_of_arrival'];
        $airline = $data['airline'];
        $price = $data['price'];
        $count = $data['count_tickets'];
        if (!$id) {
            $sql = "insert into flyshop.flight(WHENCE,
                          WHITHER,
                          DEPARTURE_DATE,
                          DATE_OF_ARRIVAL,
                          AIRLINE, PRICE,
                          COUNT_TICKETS,
                          FLAG_VIEW,
                          DEPARTURE_TIME,
                          TIME_OF_ARRIVAL)
                          VALUES('{$whence}','{$where}','{$departure_date}','{$date_of_arrival}','{$airline}','{$price}', '{$count}' , 1,'{$departure_time}','{$time_of_arrival}')";

        } else {
            $sql = "update flyshop.flight set
              WHENCE = '{$whence}',
              WHITHER = '{$where}',
              DEPARTURE_DATE = '{$departure_date}',
              DATE_OF_ARRIVAL = '{$date_of_arrival}',
              AIRLINE = '{$airline}',
              PRICE = '{$price}',
              COUNT_TICKETS = '{$count}',
              DEPARTURE_TIME = '{$departure_time}',
              TIME_OF_ARRIVAL = '{$time_of_arrival}' WHERE ID = '{$id}'";

        }
        $this->db->query($sql);
    }

    public function delete($id)
    {
        $sql = "DELETE from flyshop.flight WHERE ID = '{$id}'";
        return $this->db->query($sql);

    }

    public function get_element($id)
    {
        $sql = "select * from flyshop.flight WHERE ID = '{$id}'";
        return $this->db->query($sql)[0];
    }

    public function lastFlight($limit = '')
    {
        $sql = "select * from flyshop.flight ";
        if ($limit) {
            $sql .= $limit;
        }
        return $this->db->query($sql);
    }

    public function reservation($data)
    {
        $fio = $data['fio'];
        $email = $data['email'];
        $count_tickets = $data['count_tickets'];
        $ticket = $data['id'];
        $sql = "insert into flyshop.reservation_list(USER, COUNT_TICKETS_USER, TICKET, email) VALUES 
                ('{$fio}','{$count_tickets}',$ticket,'{$email}')";
        $sql2 = "update flyshop.flight set COUNT_TICKETS = COUNT_TICKETS - {$count_tickets} WHERE ID = {$ticket}";

        $this->db->query($sql);
        $this->db->query($sql2);
    }

    public function buy($data)
    {
        $fio = $data['fio'];
        $tel = $data['tel'];
        $email = $data['email'];
        $count_tickets = $data['count_tickets'];
        $ticket = $data['id'];
        $sql = "insert into flyshop.buy_list(USER, COUNT_TICKETS_USER, TICKET, email, tel) VALUES 
                ('{$fio}','{$count_tickets}',$ticket,'{$email}', '{$tel}')";
        $sql2 = "update flyshop.flight set COUNT_TICKETS = COUNT_TICKETS - {$count_tickets} WHERE ID = {$ticket}";

        $this->db->query($sql);
        $this->db->query($sql2);
    }

    public function lastOperations()
    {
        $sql = "SELECT rl.id, rl.USER, fl.WHENCE, fl.WHITHER,fl.DEPARTURE_DATE,fl.COUNT_TICKETS FROM flyshop.reservation_list rl LEFT JOIN flyshop.flight fl on `rl`.TICKET = fl.ID ORDER BY rl.ID DESC";
        return $this->db->query($sql);
    }
}