<?php


class Project_model extends Model
{
    public function get_all_alias()
    {
        $sql = "SELECT alias FROM db_museumgimn1.project";
        return $this->db->query($sql);
    }

    public function save($data)
    {
        //alias
        $alias = $data['alias'];

        //img для баннера
        $bd_path_img = '/img/project/' . $alias;
        $file_path_img = ROOT . '/webroot' . $bd_path_img;

        $ext = array_pop(explode('.', $_FILES['img']['name'])); // расширение
        $new_name = time() . '.' . $ext; // новое имя с расширением

        $file_full_path = $file_path_img . '/' . $new_name;

        $path_main_img_cropped = $file_path_img . '/crop' . $new_name;
        $path_bd_main_img_cropped = $bd_path_img . '/crop' . $new_name;
        if (!is_dir($file_path_img)) {
            mkdir($file_path_img);

        }
        if ($_FILES['img']['error'] == 0) {
            if (move_uploaded_file($_FILES['img']['tmp_name'], $file_full_path)) {
                $this->resize($file_full_path, $path_main_img_cropped, 1000);
                unlink($file_full_path);
            } else {
                echo 'не загрузилось';
            }
        }

        //title

        $title = $data['title'];
        $content = $data['content'];
        $content_cropped = $this->cutStr($content);
        $date = date('Y-m-d');
        //img для gallery

        $bd_path_img = '/img/project/' . $alias . "/";
        $file_path_img = ROOT . '/webroot' . $bd_path_img . "/";
        $paths = '';
        //загрузка img для гал.
        $fileElementName = 'fileToUpload';
        $files_count = sizeof($_FILES[$fileElementName]["name"]);

        for ($i = 0; $i < $files_count; $i++) {
            if (!empty($_FILES[$fileElementName]['error'][$i])) {
                switch ($_FILES[$fileElementName]['error'][$i]) {

                    case '1':
                        $error = 'размер загруженного файла превышает размер установленный параметром upload_max_filesize  в php.ini ';
                        break;
                    case '2':
                        $error = 'размер загруженного файла превышает размер установленный параметром MAX_FILE_SIZE в HTML форме. ';
                        break;
                    case '3':
                        $error = 'загружена только часть файла ';
                        break;
                    case '4':
                        $error = 'файл не был загружен (Пользователь в форме указал неверный путь к файлу). ';
                        break;
                    case '6':
                        $error = 'неверная временная дирректория';
                        break;
                    case '7':
                        $error = 'ошибка записи файла на диск';
                        break;
                    case '8':
                        $error = 'загрузка файла прервана';
                        break;
                    case '999':
                    default:
                        $error = 'No error code avaiable';
                }
            } elseif (empty($_FILES[$fileElementName]['tmp_name'][$i]) || $_FILES[$fileElementName]['tmp_name'][$i] == 'none') {
                $error = 'No file was uploaded..';
            } else {
                $new_name = uniqid() . '.jpg';
                move_uploaded_file($_FILES[$fileElementName]['tmp_name'][$i], $file_path_img . $new_name);
                $this->resize($file_path_img . $new_name, $file_path_img . $new_name, 1280);
                $full_path_bd_img = $bd_path_img . $new_name;
                $paths .= $full_path_bd_img . ';';
                @unlink($_FILES[$fileElementName][$i]);
            }
        }
        $sql = "insert into db_museumgimn1.project 
                    (alias, title, content, content_cropped, main_img_cropped, img_for_gallery, date_article) 
                    VALUES ('${alias}','${title}','${content}','${content_cropped}','${path_bd_main_img_cropped}','${paths}','${date}')";
        return $this->db->query($sql);

    }

    public function removeDirectory($dir)
    {
        if ($objs = glob($dir . "/*")) {
            foreach ($objs as $obj) {
                is_dir($obj) ? removeDirectory($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }

    public function get_alias_from_id($id)
    {
        $sql = "select alias from db_museumgimn1.project WHERE id = '${id}'";
        return $this->db->query($sql)[0]['alias'];
    }

    public function get_list($limit = false, $only_published = false)
    {
        $sql = "select * from db_museumgimn1.project WHERE 1";
        if ($limit) {
            $sql .= " {$limit}";
        }
        return $this->db->query($sql);
    }

    public function getByAlias($alias)
    {
        $alias = $this->db->escape($alias);
        $sql = "select * from db_museumgimn1.project WHERE alias = '{$alias}' limit 1";
        $result = $this->db->query($sql);
        return isset($result[0]) ? $result[0] : null;
    }

    public function getById($id)
    {
        $sql = "select * from db_museumgimn1.project WHERE id = '{$id}' limit 1";
        $result = $this->db->query($sql);
        return isset($result[0]) ? $result[0] : null;
    }

    public function edit($data, $id = null)
    {
        $id = (int)$id;
        $title = $this->db->escape($data['title']);
        $content = $this->db->escape($data['content']);
        $date_article = $this->db->escape($data['date']);
        print_r($date_article);
        $content_sub = $this->cutStr($content);
        $sql = "
            update  db_museumgimn1.project
            set title = '{$title}',
                content = '{$content}',
                content_cropped = '${content_sub}',
                date_article = '${date_article}'
                WHERE  id = {$id}  
                ";
        return $this->db->query($sql);
    }

    public function delete($id)
    {
        $id = (int)$id;
        $sql = "delete from db_museumgimn1.project WHERE id = {$id}";
        return $this->db->query($sql);
    }

    public function get_all_count()
    {
        $sql = "SELECT COUNT(*) FROM db_museumgimn1.project";
        $result = $this->db->query($sql);
        return $result[0]['COUNT(*)'];
    }

    function cutStr($str, $length = 700, $postfix = '...')
    {
        if (strlen($str) <= $length)
            return $str;

        $temp = substr($str, 0, $length);
        return substr($temp, 0, strrpos($temp, ' ')) . $postfix;
    }

    private function resize($image, $file_path_save_img, $w_o = false, $h_o = false)
    {
        if (($w_o < 0) || ($h_o < 0)) {
            echo "Некорректные входные параметры";
            return false;
        }
        list($w_i, $h_i, $type) = getimagesize($image); // Получаем размеры и тип изображения (число)
        $types = array("", "gif", "jpeg", "png"); // Массив с типами изображений
        $ext = $types[$type]; // Зная "числовой" тип изображения, узнаём название типа
        if ($ext) {
            $func = 'imagecreatefrom' . $ext; // Получаем название функции, соответствующую типу, для создания изображения
            $img_i = $func($image); // Создаём дескриптор для работы с исходным изображением
        } else {
            echo 'Некорректное изображение'; // Выводим ошибку, если формат изображения недопустимый
            return false;
        }
        /* Если указать только 1 параметр, то второй подстроится пропорционально */
        if (!$h_o) $h_o = $w_o / ($w_i / $h_i);
        if (!$w_o) $w_o = $h_o / ($h_i / $w_i);
        $img_o = imagecreatetruecolor($w_o, $h_o); // Создаём дескриптор для выходного изображения
        imagecopyresampled($img_o, $img_i, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i); // Переносим изображение из исходного в выходное, масштабируя его
        $func = 'image' . $ext; // Получаем функция для сохранения результата
        return $func($img_o, $file_path_save_img); // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
    }
}